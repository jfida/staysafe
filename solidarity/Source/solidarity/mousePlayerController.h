// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "mousePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SOLIDARITY_API AmousePlayerController : public APlayerController
{
	GENERATED_BODY()

	AmousePlayerController();
	
};

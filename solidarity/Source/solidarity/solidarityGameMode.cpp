// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "solidarityGameMode.h"
#include "solidarityCharacter.h"
#include "mousePlayerController.h"
#include "BlockActor.h"
#include <Runtime/Engine/Public/EngineUtils.h>

AsolidarityGameMode::AsolidarityGameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = AsolidarityCharacter::StaticClass();

	PlayerControllerClass = AmousePlayerController::StaticClass();




}

void AsolidarityGameMode::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);
}

void AsolidarityGameMode::RestartGame() {
    // Delete spawned blocks
    auto World = GetWorld();
    for (TActorIterator<ABlockActor> ActorItr(World); ActorItr; ++ActorItr)
    {
        ABlockActor *block = *ActorItr;
        block->Reset();
    }

    for (TActorIterator<AsolidarityCharacter> ActorItr(World); ActorItr; ++ActorItr)
    {
        AsolidarityCharacter *character = *ActorItr;
        character->Reset();
    }

    for (TActorIterator<ACameraDirector> ActorItr(World); ActorItr; ++ActorItr)
    {
        ACameraDirector *cameraDirector = *ActorItr;
        cameraDirector->Reset();
    }

}
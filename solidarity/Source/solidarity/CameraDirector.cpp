// Fill out your copyright notice in the Description page of Project Settings.


#include "CameraDirector.h"
#include "Camera/CameraComponent.h"

// Sets default values
ACameraDirector::ACameraDirector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CameraInitialStep = 0.1f;
    CameraStep = CameraInitialStep;
    CameraStepIncrease = 0.001f;
    CameraMaxStep = 5.0f;
    DeltaTimeAcc = 0.0f;
//    if (CameraOne != nullptr)
    CameraInitialLocation = FVector(-1990.0f, -300.0f, 520.0);
}

// Called when the game starts or when spawned
void ACameraDirector::BeginPlay()
{
	Super::BeginPlay();
	
}


void ACameraDirector::Reset() {
//    CameraOne->Reset();
    CameraStep = CameraInitialStep;
    DeltaTimeAcc = 0;
//    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Reset camera, %f, %f, %f"), CameraInitialLocation.X, CameraInitialLocation.Y, CameraInitialLocation.Z));
    CameraOne->SetActorLocation(FVector(-1990.0f, -300.0f, 520.0));
//    Reset();
}

// Called every frame
void ACameraDirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (DeltaTimeAcc > 0.005f) {
        FVector OldPosition = CameraOne->GetActorLocation();

        if (OldPosition.X < 9900.0f)
            OldPosition.X += CameraStep;
        CameraOne->SetActorLocation(OldPosition);
        CameraStep += CameraStepIncrease;
        if (CameraStep > CameraMaxStep) CameraStep = CameraMaxStep;
        DeltaTimeAcc = 0;
	}
	DeltaTimeAcc += DeltaTime;
}

FVector ACameraDirector::GetLocation()
{
	return CameraOne->GetActorLocation();
}

float ACameraDirector::GetWidth()
{
	return CameraOne->GetCameraComponent()->OrthoWidth;
}

float ACameraDirector::GetHeight()
{
	return GetWidth() / CameraOne->GetCameraComponent()->AspectRatio;
}

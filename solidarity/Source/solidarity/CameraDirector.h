// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Camera/CameraActor.h"
#include "CameraDirector.generated.h"

UCLASS()
class SOLIDARITY_API ACameraDirector : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraDirector();
	FVector GetLocation();
	float GetWidth();
	float GetHeight();
	void ResetCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Reset() override;

    UPROPERTY(EditAnywhere)
    ACameraActor* CameraOne;

    UPROPERTY(EditAnywhere)
    float CameraInitialStep;

    UPROPERTY(EditAnywhere)
    float CameraStepIncrease;

    UPROPERTY(EditAnywhere)
    float CameraMaxStep;


    float CameraStep;

    float DeltaTimeAcc;

    FVector CameraInitialLocation;


};

// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "solidarityCharacter.h"
//#include <Editor/UnrealEd/Public/Editor.h>
#include "PaperFlipbookComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Camera/CameraComponent.h"
#include "BlockActor.h"
#include "solidarityGameMode.h"

DEFINE_LOG_CATEGORY_STATIC(SideScrollerCharacter, Log, All);

//////////////////////////////////////////////////////////////////////////
// AsolidarityCharacter

AsolidarityCharacter::AsolidarityCharacter()
{
	// Use only Yaw from the controller and ignore the rest of the rotation.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Set the size of our collision capsule.
	GetCapsuleComponent()->SetCapsuleHalfHeight(96.0f);
	GetCapsuleComponent()->SetCapsuleRadius(40.0f);

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 500.0f;
	CameraBoom->SocketOffset = FVector(0.0f, 0.0f, 75.0f);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	
	GetCharacterMovement()->bOrientRotationToMovement = false;

	// Configure character movement
	GetCharacterMovement()->GravityScale = 2.0f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.0f;
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	GetCharacterMovement()->MaxFlySpeed = 600.0f;

	// Lock character motion onto the XZ plane, so the character can't move in or out of the screen
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->SetPlaneConstraintNormal(FVector(0.0f, -1.0f, 0.0f));

	// Behave like a traditional 2D platformer character, with a flat bottom instead of a curved capsule bottom
	// Note: This can cause a little floating when going up inclines; you can choose the tradeoff between better
	// behavior on the edge of a ledge versus inclines by setting this to true or false
	GetCharacterMovement()->bUseFlatBaseForFloorChecks = true;

    // 	TextComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("IncarGear"));
    // 	TextComponent->SetRelativeScale3D(FVector(3.0f, 3.0f, 3.0f));
    // 	TextComponent->SetRelativeLocation(FVector(35.0f, 5.0f, 20.0f));
    // 	TextComponent->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));
    // 	TextComponent->SetupAttachment(RootComponent);

	// Enable replication on the Sprite component so animations show up when networked
	GetSprite()->SetIsReplicated(true);
	bReplicates = true;
    bCanSpawnItem = true;
    SpawnCooldown = 1.0f;
    bIgnoreDeath = false;
    // 2232, -13.4, 275
    InitialLocation = FVector(-2232.0, -13.4, 275.0);
}

//////////////////////////////////////////////////////////////////////////
// Animation

void AsolidarityCharacter::UpdateAnimation()
{
	const FVector PlayerVelocity = GetVelocity();
	const float PlayerSpeedSqr = PlayerVelocity.SizeSquared();

	UPaperFlipbook* DesiredAnimation = (PlayerSpeedSqr > 0.0f) ? RunningAnimation : IdleAnimation;
	auto Movement = GetCharacterMovement();
	if (Movement->IsFalling())
        DesiredAnimation = FallingAnimation;

	// Are we moving or standing still?
	if( GetSprite()->GetFlipbook() != DesiredAnimation 	)
	{
		GetSprite()->SetFlipbook(DesiredAnimation);
	}
}

void AsolidarityCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	UpdateCharacter();	
}


void AsolidarityCharacter::OnSpawnCooldownEnd() {
//    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("END COOLDOWN")));
    bCanSpawnItem = true;
}


//////////////////////////////////////////////////////////////////////////
// Input

void AsolidarityCharacter::SpawnActor() {
    APlayerController* PlayerController = Cast<APlayerController>(GetController());
    if (PlayerController != nullptr && bCanSpawnItem)
    {
        // Get the coordinates of the mouse from our controller
        float LocationX;
        float LocationY;
        PlayerController->GetMousePosition(LocationX, LocationY);
        FVector2D ScreenPosition(LocationX, LocationY);

        FVector WorldPosition;
        FVector WorldDirection;
        PlayerController->DeprojectScreenPositionToWorld(ScreenPosition.X, ScreenPosition.Y, WorldPosition, WorldDirection);
        WorldPosition.Y = 0;
        FTransform Transform(WorldPosition);
        GetWorld()->SpawnActor(ABlockActor::StaticClass(), &Transform);
        GetWorld()->GetTimerManager().SetTimer(SpawnCooldownTimerHandle, this, &AsolidarityCharacter::OnSpawnCooldownEnd, SpawnCooldown, false);
        bCanSpawnItem = false;
    }
}

void AsolidarityCharacter::EndGame() {
    FGenericPlatformMisc::RequestExit(false);
}

void AsolidarityCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Note: the 'Jump' action and the 'MoveRight' axis are bound to actual keys/buttons/sticks in DefaultInput.ini (editable from Project Settings..Input)
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Exit", IE_Released, this, &AsolidarityCharacter::EndGame);
	PlayerInputComponent->BindAxis("MoveRight", this, &AsolidarityCharacter::MoveRight);
	PlayerInputComponent->BindAction("Spawn", IE_Pressed, this, &AsolidarityCharacter::SpawnActor);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &AsolidarityCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AsolidarityCharacter::TouchStopped);
}

void AsolidarityCharacter::MoveRight(float Value)
{
	/*UpdateChar();*/

	// Apply the input to the character motionB
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
}

void AsolidarityCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// Jump on any touch
	Jump();
}

void AsolidarityCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// Cease jumping once touch stopped
	StopJumping();
}

void AsolidarityCharacter::Reset() {
    StopJumping();
    SetActorLocation(InitialLocation);
}

void AsolidarityCharacter::UpdateCharacter()
{

	if (Camera != nullptr) {
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Player (%f, %f)"), GetActorLocation().X, GetActorLocation().Z));
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Camera (%f, %f)"), Camera->GetLocation().X, Camera->GetLocation().Z));
	}

	if (!bIgnoreDeath || GetActorLocation().X < Camera->GetLocation().X - Camera->GetWidth() / 2 ||
		GetActorLocation().Z < Camera->GetLocation().Z - Camera->GetHeight() / 2) {
		auto GameMode = GetWorld()->GetAuthGameMode<AsolidarityGameMode>();
        GameMode->RestartGame();
        bIgnoreDeath = true;
	}

	// Update animation to match the motion
	UpdateAnimation();

	// Now setup the rotation of the controller based on the direction we are travelling
	const FVector PlayerVelocity = GetVelocity();
	float TravelDirection = PlayerVelocity.X;
	// Set the rotation so that the character faces his direction of travel.
	if (Controller != nullptr)
	{
		if (TravelDirection < 0.0f)
		{
			Controller->SetControlRotation(FRotator(0.0, 180.0f, 0.0f));
		}
		else if (TravelDirection > 0.0f)
		{
			Controller->SetControlRotation(FRotator(0.0f, 0.0f, 0.0f));
		}
	}
}
